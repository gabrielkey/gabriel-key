+++
title = 'GTR R32'
date = 2024-10-05T14:53:09-03:00
draft = false
+++
O Nissan GT-R R32, frequentemente chamado de "Godzilla", é um dos carros esportivos mais icônicos da história automobilística. Produzido entre 1989 e 1994, o R32 foi uma verdadeira revolução na engenharia automotiva e se destacou principalmente por sua combinação de potência, tecnologia avançada e desempenho excepcional nas pistas.

Equipado com um motor RB26DETT de 2,6 litros e seis cilindros em linha, o R32 foi um dos primeiros carros a incorporar a tecnologia de tração integral ATTESA E-TS, que otimizava a distribuição de torque entre as rodas dianteiras e traseiras, melhorando a estabilidade e o controle em alta velocidade. Sua potência era impressionante para a época, com cerca de 280 cv, permitindo que o R32 alcançasse velocidades superiores a 250 km/h.

O design do R32 é caracterizado por linhas agressivas e aerodinâmicas, com uma frente imponente e uma traseira robusta, que o tornam facilmente reconhecível. Além de seu desempenho em pista, o GT-R R32 se tornou um ícone na cultura de carros japoneses, sendo amplamente celebrado por entusiastas e colecionadores.

A fama do R32 foi consolidada em competições de turismo, onde se destacou em campeonatos, incluindo o All-Japan GT Championship. Sua influência se estendeu até os dias atuais, com muitos carros modernos da Nissan herdando seu legado de performance e inovação. O Nissan GT-R R32 permanece como um símbolo de excelência na engenharia automotiva e uma lenda entre os apaixonados por velocidade.