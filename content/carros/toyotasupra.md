+++
title = 'Toyota Supra'
date = 2024-09-21T22:36:08-03:00
draft = false
+++
O Toyota Supra é um icônico carro esportivo japonês que conquistou a fama mundial por sua performance, design arrojado e rica história. Produzido pela primeira vez em 1978, o Supra passou por diversas gerações, com a quarta geração (A80), lançada em 1993, sendo particularmente célebre. Esta versão se destacou pelo seu motor turboalimentado de 3,0 litros, que oferecia uma impressionante potência e torque, tornando-o um favorito entre entusiastas de carros e amantes de tunning.

O Supra é frequentemente associado à cultura automobilística japonesa e é amplamente reconhecido por suas aparições em filmes e jogos, como a série "Velozes e Furiosos". Em 2020, a Toyota lançou a quinta geração do Supra (A90), desenvolvida em parceria com a BMW, mantendo a essência esportiva do modelo enquanto incorporava tecnologias modernas e um design mais contemporâneo.

Além de seu desempenho excepcional, o Supra é conhecido por sua capacidade de modificação, permitindo que os proprietários personalizem o carro para aumentar ainda mais sua potência e desempenho. Sua combinação de estilo, velocidade e herança cultural solidificou o Toyota Supra como um dos carros esportivos mais reverenciados do mundo.
