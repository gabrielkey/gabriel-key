+++
title = 'Curriculo'
date = 2024-09-21T19:27:10-03:00
draft = false
+++

## Dados Pessoais
- **Nome:** Gabriel Key Shimada Matsuda
- **Endereço:** Alameda dos Arapanés
- **Telefone:** (11) 96133-2868
- **E-mail:** gabrielkey@usp.br
-



## Formação Acadêmica
- **Engenharia Mecatrônica** - USP, 2026

## Habilidades Técnicas
- **Linguagens de Programação:**  JavaScript, Python, HTML/CSS
- **Outras Habilidades:** Pacote Office


## Idiomas
- **Inglês** - Avançado

## Projetos Relevantes
- **Baja**: Competição de carros 4x2 e 4x4.